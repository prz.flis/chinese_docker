#!/bin/sh

docker build -f main.dockerfile -t ccr/server/main .
docker build -f preprocessing.dockerfile -t ccr/server/preprocessing .
docker build -f recognition.dockerfile -t ccr/server/recognition .
docker build -f translation.dockerfile -t ccr/server/translation .
docker build -f web_service.dockerfile -t ccr/server/web_service .
docker build -f db_controller.dockerfile -t ccr/server/db_controller .
