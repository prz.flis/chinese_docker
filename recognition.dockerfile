# Use an official Python runtime as a parent image
FROM python:3.6

RUN mkdir -p /usr/server

ADD server/ /usr/server

RUN pip install -r /usr/server/requirements.txt

WORKDIR /usr/server

ENTRYPOINT [ "/usr/local/bin/python", "/usr/server/recognition_instance.py" ]
